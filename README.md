# D3 Test

## Running
Any commands prefaced with `$` should be run in your shell

* Install node/npm
* Install grunt (`$ npm install -g grunt`)
* Install bower (`$ npm install -g bower`)
* `$ git clone git@bitbucket.org:switz/d3-test.git`
* `$ cd d3-test`
* `$ npm install`
* `$ bower install`
* `$ grunt compile`
* `$ cd public`
* `$ python -m SimpleHTTPServer 3000`
* Navigate your browser to http://localhost:3000
* Enjoy

Here's a copy/pastable list of commands your can throw in your shell

```
npm install -g grunt
npm install -g bower
git clone git@bitbucket.org:switz/d3-test.git
cd d3-test
npm install
bower install
grunt compile
cd public
python -m SimpleHTTPServer 3000
```
