# This is the main application configuration file.  It is a Grunt
# configuration file, which you can learn more about here:
# https://github.com/cowboy/grunt/blob/master/docs/configuring.md
module.exports = (grunt) ->
  grunt.initConfig

    copy:
      app:
        files: [
          src: 'index.html', dest: 'public/'
        ]
      vendor:
        files: [
          { src: 'bower_components/d3/d3.js', dest: 'public/js/d3.js' }
          { src: 'bower_components/topojson/topojson.js', dest: 'public/js/topojson.js' }
          { src: ['json/**'], dest: 'public/' }
        ]

    coffee:
      compile:
        files:
          "public/js/scripts.js": ["src/**/*.coffee"]

    # The stylus task is used to compile Stylus stylesheets into a single
    # CSS file for debug and release deployments.
    stylus:
      compile:
        files:
          "public/css/index.css": [
            "styles/index.styl"
          ]

    # The watch task can be used to monitor the filesystem and execute
    # specific tasks when files are modified.  By default, the watch task is
    # available to compile stylus templates if you are unable to use the
    # runtime builder (use if you have a custom server, PhoneGap, Adobe Air,
    # etc.)
    watch:
      compile:
        files: ["src/**/*.coffee"]
        tasks: ["coffee"]

      stylus:
        files: ["styles/**/*.styl"]
        tasks: ["stylus"]

  grunt.loadNpmTasks "grunt-contrib-watch"
  grunt.loadNpmTasks "grunt-contrib-coffee"
  grunt.loadNpmTasks "grunt-contrib-stylus"
  grunt.loadNpmTasks "grunt-contrib-copy"

  grunt.registerTask "compile", ["coffee", "stylus", "copy"]
