width = 960
height = 960

projection = d3.geo.mercator()
              .scale((width + 1) / 2 / Math.PI)
              .translate([ width / 2, height / 2 ])
              .precision(.1)

path = d3.geo.path()
        .projection(projection)

graticule = d3.geo.graticule()

svg = d3.select("body")
        .append("svg")
        .attr("width", width)
        .attr("height", height)

# Insert graticule
svg.append("path")
  .datum(graticule)
  .attr("class", "graticule")
  .attr("d", path)

$ ->
  # Initialize and cache jQuery elements
  $panel       = $('#info-panel')
  $city        = $panel.find '.city'
  $description = $panel.find '.description'
  $population  = $panel.find '.population'


  # Get the world data
  d3.json "/json/world.json", (error, world) ->

    # Insert land
    svg.insert("path", ".graticule")
      .datum(topojson.feature(world, world.objects.land))
      .attr("class", "land")
      .attr("d", path)

    # Insert boundaries
    svg.insert("path", ".graticule")
      .datum(topojson.mesh(world, world.objects.countries, (a, b) -> a isnt b))
      .attr("class", "boundary")
      .attr("d", path)

    # Get the city data
    d3.csv "json/cities.csv", (error, data) ->
      svg.selectAll("circle")
        .data(data)
        .enter()
        .append("circle")
        .attr("cx", (d) -> projection([ d.lon, d.lat ])[0])
        .attr("cy", (d) -> projection([ d.lon, d.lat ])[1])
        .attr("r", 7)
        .style("fill", (d) -> d.color)
        # When clicking on a city circle, update the info panel
        .on 'click', (d, i) ->
          $city.html d.city
          $description.html d.description
          $population.html d.population
          $panel.show()

  d3.select(self.frameElement).style "height", height + "px"